Projekt 1 Linux i Python

Skrypt ma za zadanie ustalić adres IP oraz maskę podsieci, na podstawie powyższych informacji przeskanować sieć  i ustalić adres IP i MAC celu. Następnie ustala otwarte porty celu. Kolejno pobiera baner serwera (Banner Grabbing). Na końcu dokonuje ataku Brute-Force na podstawie słowiników.

Skrypt wykorzystuje biblioteki:
- scapy
- socket
- paramiko

Program w pierwszej kolejnosci pobiera tablicę ARP, następnie na podstawie wyciagniętego adresu IP z tablicy ARP skanuje sieć pod względem innych tablic ARP. Kolejno wyszukuje otwartych portów (21 - ftp, 22 - ssh, 80 - http). Na podstawie portu 80 pobiera baner celu. Ostatnim elementem jest przeprowadzenie ataku Brute-Force.
